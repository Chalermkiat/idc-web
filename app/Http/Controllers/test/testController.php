<?php

namespace App\Http\Controllers\test;

//use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Request;

class testController extends Controller
{
    public function index()
    {
    	return view('idc_index');
    }

    public function testPage()
    {
    	//passing Data to views
    	$name = 'bob <span style="color:red;"> ba </span>';
    	$last = 'blast';
    	// return view('pages.web1')->with([
    	// 	'name' => 	'bob',
    	// 	'last' =>	'blast'
    	// 	]);
    	return view('testpages.web1',compact('name','last'));
    }

    public function formPage()
    {
    	return view('testpages.form');
    }

    public function store()
    {
    	//$input = Request::all();
    	$input = Request::get('title');

    	return $input;
    }
}

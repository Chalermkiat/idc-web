<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Idc_test;

class idc_controller extends BaseController
{
	use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


	public function redirect()
	{
		return redirect('admin');
	}
	public function index()
	{
		
		return view('pages.idc_index'); 
	}

	public function page($page)
	{
		$idc_test = \App\Idc_test::all();
		return view('pages.'.$page , compact('idc_test'));
	}


}


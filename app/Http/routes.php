<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'idc_controller@redirect');
Route::get('/admin', 'idc_controller@index');
Route::get('/admin/{page}', 'idc_controller@page');



//Route::get('/idc_index','idc_controller@page');

// ==============for Test===================
Route::get('/test', 'test\testController@testPage');
Route::get('/form', 'test\testController@formPage');
Route::post('/test', 'test\testController@store');

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});

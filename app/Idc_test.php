<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Idc_test extends Model
{
    	 /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'index', 'title', 'body1','body2','body3'
    ];
 	

 	/**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
  /*  protected $hidden = [
    	'password'
    ];*/
}

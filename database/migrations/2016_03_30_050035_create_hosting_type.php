<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHostingType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('idc_hosting_type', function (Blueprint $table) {
            $table->increments('index');
            $table->string('title');
            $table->text('body1');
            $table->text('body2');
            $table->text('body3');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('idc_hosting_type');
    }
}

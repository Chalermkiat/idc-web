<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Model::unguard();

    	// $user = factory(App\User::class)->create([
    	// 	'name' => 'Abigail',
    	// 	]);
    	
    	// $user = factory(App\User::class, 30)->create();

        //$this->call(JokesTableSeeder::class);
        //$this->call(UsersTableSeeder::class);

        $this->call(IDCTableSeeder::class);
        
    	Model::reguard();
    }
}

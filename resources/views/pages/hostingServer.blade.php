@extends('header_footer')

@section('title')
CAT Telecom
@stop

@section('content')


<table class="table table-bordered table-hover table-condensed" id="index_table">
	<thead>
		<tr class="info"><th id="MainMenu_index"> Maintain Server (จัดการข้อมูลของServer)</th></tr>
	</thead>
	<tbody>
		<tr class="active boldIndexMenu">
			<td> 

				<!-- input form -->
				<form class="form-horizontal">
					<div class="form-group">
					<label for="serverGroupName" class="col-sm-3 control-label">Server Group Name</label>
						<div class="col-sm-9">
							<select class="form-control" name="serverGroupName">
								<option value="1">Windows Server</option>
								<option value="2">HP-UNIX-Iplanet Server</option>
								<option value="3">none</option>
								<option value="4">HP-UNIX-Apache Server</option>
							</select>
						</div>
					</div>
					<div class="form-group">
					<label for="ServerName" class="col-sm-3 control-label">Server Name</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="ServerName" placeholder="Server Name">
						</div>
					</div>
					<div class="form-group">
						<label for="IPAddress" class="col-sm-3 control-label">IP Address</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="IPAddress" placeholder="IP Address">
						</div>
					</div>
					<!-- BUTTON -->
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-3">
							<button type="submit" class="btn btn-info btn-block">CREATE</button>
						</div>
						<div class="col-sm-3">
							<button type="submit" class="btn btn-default btn-block">CANCEL</button>
						</div>
					</div>
				</form>

			</td>
		</tr>

		<tr class="info">
			<td>
				<!-- ALL Check box -->
				<input type="checkbox" name="All" value="all" />
				 <strong>Server</strong> 
			</td>
		</tr>

		@foreach($idc_test as $test)
		<tr>
			<td>
			<input type="checkbox" name="selectedID" value="{{ $test->index}}" />
				<a href="#">{{ $test->title}}</a>
			</td>
		</tr>
		@endforeach

		<tr>
			<td>
				<button type="submit" class="btn btn-danger btn-block disabled">DELETE</button>
			</td>
		</tr>

	</tbody>
</table>






@stop
@extends('header_footer')

@section('title')
CAT Telecom
@stop

@section('content')


<table class="table table-bordered table-hover table-condensed" id="index_table">
	<thead>
		<tr class="info"><th id="MainMenu_index" colspan="5"> Maintain Standard Service Item (จัดการข้อมูลของรายการบริการมาตรฐาน)</th></tr>
	</thead>
	<tbody>
		<tr class="active boldIndexMenu">
			<td colspan="5"> 
				<!-- *********SEARCH************** 
				******** HAS s AFTER ID***********
				**********************************-->

				<!-- input form -->
				<form class="form-horizontal">
					<div class="form-group">
						<h3>Search Function</h3>
						<label class="col-sm-3 control-label">Std. Service Group <br/>(กลุ่มบริการมาตรฐาน)</label>
						<div class="col-sm-9">
							<select class="form-control" id="StdServGroupS">
								@foreach($idc_test as $test)
									<option value="{{$test->index}}"> {{$test->title}}</option>
								@endforeach
		
							</select>
					
						</div>
					</div>
				
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-3">
							<button type="submit" class="btn btn-info btn-block">SEARCH</button>
						</div>
					</div>
				</form>

			</td>
		</tr>

		<tr class="active boldIndexMenu">
			<td colspan="5"> 
				<!-- ***********INPUT FORM************ -->

				<!-- input form -->
				<form class="form-horizontal">
					<!-- Std.service Group -->
					<div class="form-group">
						<h3>Input Form</h3>
						<label class="col-sm-3 control-label">Std. Service Group <br/>(กลุ่มบริการมาตรฐาน)</label>
						<div class="col-sm-9">
							<select class="form-control" id="StdServGroupS">
								@foreach($idc_test as $test)
									<option value="{{$test->index}}"> {{$test->title}}</option>
								@endforeach
							</select>
						</div>
					</div>
					<!-- ITEM NAME -->
					<div class="form-group">
						<label  class="col-sm-3 control-label">Item Name <br/>(ชื่อบริการ)</label>
						<label class="col-sm-1 control-label">EN</label>
						<div class="col-sm-3">
							<input type="text" class="form-control" id="ItemNameEN" placeholder="ภาษาอังกฤษ">
						</div>
						<label class="col-sm-1 control-label">TH</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" id="ItemNameTH" placeholder="ภาษาไทย">
						</div>
					</div>
					<!-- ITEM CODE-->
					<div class="form-group">
						<label class="col-sm-3 control-label">Item Code (รหัสบริการ)</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="ItemCode" placeholder="Item Code">
						</div>
					</div>

					<!-- SEQUENCE ID -->
					<div class="form-group">
						<label for="SequenceID" class="col-sm-3 control-label">SequenceID (รหัสลำดับ)</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="SequenceID" placeholder="SequenceID">
						</div>
					</div>

					<!-- E-Directory Code -->
					<div class="form-group">
						<label class="col-sm-3 control-label">E-Directory Code <br/>(รหัสที่ e-Directory)</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="E-dirCode" placeholder="E-Directory Code">
						</div>
					</div>

					<!-- REMARK -->
					<div class="form-group">
						<label  class="col-sm-3 control-label">Remark (หมายเหตุ)</label>
						<label  for="RemarkEN" class="col-sm-1 control-label">EN</label>
						<div class="col-sm-3">
							<input type="text" class="form-control" id="RemarkEN" placeholder="ภาษาอังกฤษ">
						</div>
						<label  for="RemarkTH" class="col-sm-1 control-label">TH</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" id="RemarkTH" placeholder="ภาษาไทย">
						</div>
					</div>
					<!-- BUTTON  -->
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-3">
							<button type="submit" class="btn btn-info btn-block">CREATE</button>
						</div>
						<div class="col-sm-3">
							<button type="submit" class="btn btn-default btn-block">CANCEL</button>
						</div>
					</div>
				</form>

			</td>
		</tr>



		<tr class="info">
			<td>
				<!-- ALL Check box -->
				<input type="checkbox" name="All" value="all" />
				<strong>Sequence ID</strong>
			</td>
			<td>
				<strong>Item Code</strong>
			</td>
			<td>
				<strong>Item Name</strong>
			</td>
			<td>
				<strong>Unit</strong>
			</td>
			<td>
				<strong>e-Dir Code</strong>
			</td>
		</tr>

		@foreach($idc_test as $test)
		<tr>
			<td>
				<input type="checkbox" name="selectedID" value="{{ $test->index}}" />
			
				{{$test->index}}
			</td>
			<td>
				<a href="#">{{$test->body1}}</a>
			</td>
			<td>
				{{ $test->title}}
			</td>
			<td>
				1
			</td>
			<td>
				xxx
			</td>
		</tr>
		@endforeach

		<tr>
			<td colspan="5">
				<button type="submit" class="btn btn-danger btn-block disabled">DELETE</button>
			</td>
		</tr>

	</tbody>
</table>






@stop
@extends('header_footer')

@section('title')
CAT Telecom
@stop

@section('content')


<table class="table table-bordered table-hover table-condensed" id="index_table">
	<thead>
		<tr class="info"><th id="MainMenu_index" colspan="4"> Add Service Package (สร้างรายละเอียดบริการ)</th></tr>
	</thead>
	<tbody>

		<tr class="active boldIndexMenu">
			<td colspan="4"> 
				<!-- ***********INPUT FORM************ -->

				<!-- input form -->
				<form class="form-horizontal">

					<!-- Package Name -->
					<div class="form-group">
						<label class="col-sm-3 control-label">Package Name (ชื่อบริการ)</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="PackageName" placeholder="PackageName">
						</div>
					</div>

					<!-- Package Code -->
					<div class="form-group">
						<label class="col-sm-3 control-label">Package Code (รหัสบริการ)</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="PackageCode" placeholder="PackageCode">
						</div>
					</div>

					<!-- Hosting Type-->
					<div class="form-group">
						<label class="col-sm-3 control-label">Hosting Type (ประเภท Hosting)</label>
						<div class="col-sm-9">
							<select class=" form-control" id="hostingType">
								<option value="1">**********1</option>
								<option value="2">************2</option>
								<option value="3">**********3</option>
							</select>
						</div>
					</div>

					<!-- Service Group-->
					<div class="form-group">
						<label class="col-sm-3 control-label">Server Group (กลุ่มของServer)</label>
						<div class="col-sm-9">
							<select class=" form-control" id="ServiceGroup">
								<option value="1">**********1</option>
								<option value="2">************2</option>
								<option value="3">**********3</option>
							</select>
						</div>
					</div>

					<!-- Database-->
					<div class="form-group">
						<label class="col-sm-3 control-label">Database (ฐานข้อมูล)</label>
						<div class="col-sm-9">
							<select class=" form-control" id="Database">
								<option value="1">**********1</option>
								<option value="2">************2</option>
								<option value="3">**********3</option>
							</select>
						</div>
					</div>

					<!-- Platform-->
					<div class="form-group">
						<label class="col-sm-3 control-label">Platform (ระบบ)</label>
						<div class="col-sm-9">
							<select class=" form-control" id="Platform">
								<option value="1">**********1</option>
								<option value="2">************2</option>
								<option value="3">**********3</option>
							</select>
						</div>
					</div>

					<!-- SEQUENCE ID -->
					<div class="form-group">
						<label for="SequenceID" class="col-sm-3 control-label">SequenceID (รหัสลำดับ)</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="SequenceID" placeholder="SequenceID">
						</div>
					</div>

					<!-- E-Directory Code -->
					<div class="form-group">
						<label class="col-sm-3 control-label">E-Directory Code <br/>(รหัสที่ e-Directory)</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="E-dirCode" placeholder="E-Directory Code">
						</div>
					</div>

					<!-- Promotion -->
					<div class="form-group">
						<label class="col-sm-3 control-label">Promotion (มีรายการส่งเสริมการขาย)</label>
						
						<label class="col-sm-1 control-label"> From</label>
						<div class="col-sm-3">
							<div class="input-group">
						    	<span class="input-group-addon">
						        	<input type="checkbox">
						      	</span>
								<input type="date" class="form-control" >
							</div>
						</div>
						<label class="col-sm-1 control-label"> To</label>	
						<div class="col-sm-4">
							<input type="date" class="form-control" >
						</div>
					</div>

					<!-- Published -->
					<div class="form-group">
						<label class="col-sm-3 control-label">Published (ต้องการขึ้นแสดงแก่ลูกค้า)</label>
						
						<label class="col-sm-1 control-label"> From</label>
						<div class="col-sm-3">
							<div class="input-group">
						    	<span class="input-group-addon">
						        	<input type="checkbox" >
						      	</span>
								<input type="date" class="form-control"  >
							</div>
						</div>
						<label class="col-sm-1 control-label"> To</label>	
						<div class="col-sm-4">
							<input type="date" class="form-control" size="10" >
						</div>
					</div>

				</form>

			</td>
		</tr>



		<tr class="info">
			<td>
				<strong>Include?</strong>
			</td>
			<td>
				<strong>Item Code</strong>
			</td>
			<td>
				<strong>Item Name</strong>
			</td>

			<td>
				<strong>Unit</strong>
			</td>

			<td>
				<strong>e-Dir Code</strong>
			</td>
		</tr>

		@foreach($idc_test as $test)
		<tr>
			<td>
				<input type="checkbox" name="selectedID" value="{{ $test->index}}" />

				{{$test->index}}
			</td>
			<td>
				<a href="#">{{$test->body1}}</a>
			</td>
			<td>
				{{ $test->title}}
			</td>
			<td>
				1
			</td>


			<td>
				xxx
			</td>
		</tr>
		@endforeach

		<tr>
			<td colspan="5">
				<button type="submit" class="btn btn-info btn-lg disabled">Submit</button>
				<button type="submit" class="btn btn-default btn-lg">Cancel</button>
			</td>
		</tr>

	</tbody>
</table>






@stop
@extends('header_footer')

@section('title')
CAT Telecom
@stop

@section('content')


<table class="table table-bordered table-hover table-condensed" id="index_table">
	<thead>
		<tr class="info"><th id="MainMenu_index" colspan="8"> Add Service Contract (สร้างสัญญาการให้บริการใหม่)</th></tr>
	</thead>
	<tbody>
		<tr class="active boldIndexMenu">
			<td colspan="8"> 
				<!-- input form -->
				<form class="form-horizontal">

					<!-- Domain name -->
					<div class="form-group">
						<h3>Search Function</h3>
						<label for="DomainName" class="col-sm-3 control-label">Domain Name</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="DomainName" placeholder="Domain Name">
							
						</div>
					</div>

					<!-- Package From -->
					<div class="form-group">
						<label class="col-sm-3 control-label">Based Package From <br/>
(เลือกบริการ)</label>
						<div class="col-sm-9">
							<select class="form-control" id="packageFrom">
								<option>***All Status</option>
								<option>Pen**ding</option>
								<option>Appro****ved</option>
								<option>Suspene**d</option>
								<option>Expire*****d</option>
							</select>
						</div>

					</div>


					<!-- Hosting Type --> 
					<div class="form-group">
						<label for="HostingType" class="col-sm-3 control-label">Hosting Type <br/>(ประเภทของ Hosting)</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="HostingType" placeholder="Hosting Type">
						</div>
					</div>

					<!-- Note --> 
					<div class="form-group">
						<label for="Note" class="col-sm-3 control-label">Note <br/>(หมายเหตุ)</label>
						<div class="col-sm-9">
							<textarea class="form-control" id="Note" placeholder="Note" rows="3"></textarea>
						</div>
					</div>


					

					<!-- SEARCH BUTTON -->
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-3">
							<button type="submit" class="btn btn-info btn-block">SEARCH</button>
						</div>
					</div>
				</form>

			</td>
		</tr>

		<tr class="info">
			<td>
				 <strong> Domain Name </strong> 
			</td>
			<td>
				 <strong> Server </strong> 
			</td>
			<td>
				 <strong> Customer </strong> 
			</td>
			<td>
				 <strong> Start Date </strong> 
			</td>
			<td>
				 <strong> End Date </strong> 
			</td>
			<td>
				 <strong> Status </strong> 
			</td>
			<td>
				 <strong> Action </strong> 
			</td>
			<td>
				 <strong> Check </strong> 
			</td>
		</tr>

		@foreach($idc_test as $test)
		<tr>
			<td>
			<input type="checkbox" name="selectedID" value="{{ $test->index}}" />
				<a href="#">{{ $test->title}}</a>
			</td>
		</tr>
		@endforeach



		<tr>
			<td>
				<button type="submit" class="btn btn-info btn-block ">Submit</button>
			</td>
		</tr>
		<tr>
			<td>
				<button type="submit" class="btn btn-default btn-block ">Reset</button>
			</td>
		</tr>

	</tbody>
</table>






@stop
@extends('header_footer')

@section('title')
CAT Telecom
@stop

@section('content')


<table class="table table-bordered table-hover table-condensed" id="index_table">
	<thead>
		<tr class="info"><th id="MainMenu_index" colspan="3"> Maintain Service Group (จัดการข้อมูลกลุ่มของบริการ)</th></tr>
	</thead>
	<tbody>
		<tr class="active boldIndexMenu">
			<td colspan="3"> 
				<!-- *********SEARCH************** 
				******** HAS s AFTER ID***********
				**********************************-->

				<!-- input form -->
				<form class="form-horizontal">
					<div class="form-group">
						<h3>Search Function</h3>
						<label for="GroupCodeS" class="col-sm-3 control-label">Group Code (รหัสกลุ่มบริการ)</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="GroupCodeS" placeholder="Group Code">
						</div>
					</div>
					<div class="form-group">
						<label for="GroupNameS" class="col-sm-3 control-label">GroupName <br/>(ชื่อกลุ่มบริการ)</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="GroupNameS" placeholder="GroupName">
						</div>
					</div>
					<!-- SEARCH BUTTON -->
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-3">
							<button type="submit" class="btn btn-info btn-block">SEARCH</button>
						</div>
					</div>
				</form>

			</td>
		</tr>

		<tr class="active boldIndexMenu">
			<td colspan="3"> 
				<!-- ***********INPUT FORM************ -->

				<!-- input form -->
				<form class="form-horizontal">
					<div class="form-group">
						<h3>Input Form</h3>
						<!-- Group Name -->
						<label for="GroupCode" class="col-sm-3 control-label">Group Code (รหัสกลุ่มบริการ)</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="GroupCode" placeholder="Group Code">
						</div>
					</div>
					<!-- GROUP CODE -->
					<div class="form-group">
						<label  class="col-sm-3 control-label">GroupName <br/>(ชื่อกลุ่มบริการ)</label>
						
						<label  for="GroupNameEN" class="col-sm-1 control-label">EN</label>
						<div class="col-sm-3">
							<input type="text" class="form-control" id="GroupNameEN" placeholder="ภาษาอังกฤษ">
						</div>
						<label  for="GroupNameTH" class="col-sm-1 control-label">TH</label>
						<div class="col-sm-3">
							<input type="text" class="form-control" id="GroupNameTH" placeholder="ภาษาไทย">
						</div>
					</div>
					<!-- SEQUENCE ID -->
					<div class="form-group">
						<label for="SequenceID" class="col-sm-3 control-label">SequenceID (รหัสลำดับ)</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="SequenceID" placeholder="SequenceID">
						</div>
					</div>

					<!-- REMARK -->
					<div class="form-group">
						<label  class="col-sm-3 control-label">Remark (หมายเหตุ)</label>
						<label  for="RemarkEN" class="col-sm-1 control-label">EN</label>
						<div class="col-sm-3">
							<input type="text" class="form-control" id="RemarkEN" placeholder="ภาษาอังกฤษ">
						</div>
						<label  for="RemarkTH" class="col-sm-1 control-label">TH</label>
						<div class="col-sm-3">
							<input type="text" class="form-control" id="RemarkTH" placeholder="ภาษาไทย">
						</div>
					</div>
					<!-- BUTTON  -->
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-3">
							<button type="submit" class="btn btn-info btn-block">CREATE</button>
						</div>
						<div class="col-sm-3">
							<button type="submit" class="btn btn-default btn-block">CANCEL</button>
						</div>
					</div>
				</form>

			</td>
		</tr>



		<tr class="info">
			<td>
				<!-- ALL Check box -->
				<input type="checkbox" name="All" value="all" />
				<strong>Sequence ID</strong>
			</td>
			<td>
				<strong>Group Code</strong>
			</td>
			<td>
				<strong>Group Name</strong>
			</td>
		</tr>

		@foreach($idc_test as $test)
		<tr>
			<td>
				<input type="checkbox" name="selectedID" value="{{ $test->index}}" />
			
				{{$test->index}}
			</td>
			<td>
				<a href="#">{{$test->body1}}</a>
			</td>
			<td>
				{{ $test->title}}
			</td>
		</tr>
		@endforeach

		<tr>
			<td colspan="3">
				<button type="submit" class="btn btn-danger btn-block disabled">DELETE</button>
			</td>
		</tr>

	</tbody>
</table>






@stop
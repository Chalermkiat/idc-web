@extends('header_footer')

@section('title')
CAT Telecom
@stop

@section('content')


<table class="table table-bordered table-hover table-condensed" id="index_table">
	<thead>
		<tr class="info"><th id="MainMenu_index" colspan="8"> Service Contract (สัญญาการให้บริการ)</th></tr>
	</thead>
	<tbody>
	
		<tr class="active boldIndexMenu">
			<td colspan="8"> 
				
				 <a class="btn btn-default btn-block btn-lg" href="/admin/serviceContractForm">Add New Service Contract (สร้างสัญญาใหม่)</a>
			</td>
		</tr>

		<tr class="active boldIndexMenu">
			<td colspan="8"> 
				<!-- input form -->
				<form class="form-horizontal">

					<!-- Domain name -->
					<div class="form-group">
						<h3>Search Function</h3>
						<label for="DomainNameS" class="col-sm-3 control-label">Domain Name</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="DomainNameS" placeholder="Domain Name" aria-describedby="helpBlock">
							<span id="helpBlock" class=" col-sm-8 help-block">(No www: e.g. a, abc, abc.co.th, .cn.th)</span>
						</div>
					</div>

					<!-- Customer Name --> 
					<div class="form-group">
						<label for="CustomerNameS" class="col-sm-3 control-label">Customer Name <br/>(ชื่อลูกค้า)</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="CustomerNameS" placeholder="Customer Name">
						</div>
					</div>

					<!-- Status -->
					<div class="form-group">
						<label class="col-sm-3 control-label">Status (สถานะสัญญา)</label>
						<div class="col-sm-9">
							<select class="form-control" id="status">
								<option>All Status</option>
								<option>Pending</option>
								<option>Approved</option>
								<option>Suspened</option>
								<option>Expired</option>
							</select>
						</div>

					</div>


					<!-- SEARCH BUTTON -->
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-3">
							<button type="submit" class="btn btn-info btn-block">SEARCH</button>
						</div>
					</div>
				</form>

			</td>
		</tr>

		<tr class="info">
			<td>
				 <strong> Domain Name </strong> 
			</td>
			<td>
				 <strong> Server </strong> 
			</td>
			<td>
				 <strong> Customer </strong> 
			</td>
			<td>
				 <strong> Start Date </strong> 
			</td>
			<td>
				 <strong> End Date </strong> 
			</td>
			<td>
				 <strong> Status </strong> 
			</td>
			<td>
				 <strong> Action </strong> 
			</td>
			<td>
				 <strong> Check </strong> 
			</td>
		</tr>

		@foreach($idc_test as $test)
		<tr>
			<td>
			<input type="checkbox" name="selectedID" value="{{ $test->index}}" />
				<a href="#">{{ $test->title}}</a>
			</td>
		</tr>
		@endforeach

		<tr>
			<td>
				<button type="submit" class="btn btn-danger btn-block disabled">DELETE</button>
			</td>
		</tr>

	</tbody>
</table>






@stop
@extends('header_footer')

@section('title')
CAT Telecom
@stop

@section('content')


<table class="table table-bordered table-hover table-condensed" id="index_table">
	<thead>
		<tr class="info"><th id="MainMenu_index" colspan="3"> Service Package (กลุ่มบริการ)</th></tr>
	</thead>
	<tbody>

		<tr class="active boldIndexMenu">
			<td colspan="3"> 
				<a class="btn btn-default btn-block btn-lg" href="/admin/servicePackageForm">Add New Service Package (สร้างสินค้า)</a>
			</td>
		</tr>

		<tr class="info">
			<td>
				<strong>Sequence ID</strong>
			</td>
			<td>
				<strong>Name</strong>
			</td>
			<td>
				<strong>Hosting Type</strong>
			</td>
		</tr>

		@foreach($idc_test as $test)
		<tr>
			<td>
				{{$test->index}}
			</td>
			<td>
				<a href="#">{{$test->body1}}</a>
			</td>
			<td>
				{{ $test->title}}
			</td>

		</tr>
		@endforeach

	
	</tbody>
</table>






@stop
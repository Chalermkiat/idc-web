@extends('header_footer')

@section('title')
CAT Telecom
@stop

@section('content')


<table class="table table-bordered table-hover table-condensed" id="index_table">
	<thead>
		<tr class="info"><th id="MainMenu_index"> Maintain Hosting Type - จัดการข้อมูลประเภทของHosting</th></tr>
	</thead>
	<tbody>
		<tr class="active boldIndexMenu">
			<td> 

				<!-- input form -->
				<form class="form-horizontal">
					<div class="form-group">
					<label for="inputHostingEN" class="col-sm-3 control-label">ภาษาอังกฤษ</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="inputHostingEN" placeholder="EN">
						</div>
					</div>
					<div class="form-group">
						<label for="inputHostingTH" class="col-sm-3 control-label">ภาษาไทย</label>
						<div class="col-sm-9">
							<input type="text" class="form-control" id="inputHostingTH" placeholder="TH">
						</div>
					</div>
					<!-- BUTTON -->
					<div class="form-group">
						<div class="col-sm-offset-3 col-sm-3">
							<button type="submit" class="btn btn-info btn-block">CREATE</button>
						</div>
						<div class="col-sm-3">
							<button type="submit" class="btn btn-default btn-block">CANCEL</button>
						</div>
					</div>
				</form>

			</td>
		</tr>

		<tr class="info">
			<td>
				<!-- ALL Check box -->
				<input type="checkbox" name="All" value="all" />
				 <strong>Hosting Type</strong> 
			</td>
		</tr>

		@foreach($idc_test as $test)
		<tr>
			<td>
			<input type="checkbox" name="selectedID" value="{{ $test->index}}" />
				<a href="#">{{ $test->title}}</a>
			</td>
		</tr>
		@endforeach

		<tr>
			<td>
				<button type="submit" class="btn btn-danger btn-block disabled">DELETE</button>
			</td>
		</tr>

	</tbody>
</table>






@stop
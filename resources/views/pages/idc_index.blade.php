@extends('header_footer')

@section('title')
CAT Telecom
@stop

@section('content')


	<table class="table table-bordered table-hover" id="index_table">
		<thead>
			<tr class="info"><th id="MainMenu_index"> Main Menu - รายการหลัก</th></tr>
		</thead>
		<tbody>
			<tr class="active boldIndexMenu">
				<td> 
					Manage Configuration
				</td>
			</tr>
			<tr>
				<td>
					<a href="/admin/hostingType">
						<span class="glyphicon glyphicon-triangle-right"></span> 
						Maintain Hosting Type - จัดการข้อมูลประเภทของHosting
					</a>
				</td>
			</tr>
			<tr>
				<td>
					<a href="/admin/hostingServerGroup">
						<span class="glyphicon glyphicon-triangle-right"></span> 
						Maintain Server Group - จัดการข้อมูลกลุ่มของServer
					</a>
				</td>
			</tr>
			<tr>
				<td>
					<a href="/admin/hostingServer">
					<span class="glyphicon glyphicon-triangle-right"></span> 
						Maintain Server - จัดการข้อมูลของServer
					</a>
				</td>
			</tr>
			<tr>
				<td>
					<a href="/admin/hostingServiceGroup">
						<span class="glyphicon glyphicon-triangle-right"></span> 
						Maintain Service Group - จัดการข้อมูลกลุ่มของบริการ
					</a>
				</td>
			</tr>
			<tr>
				<td>
					<a href="/admin/maintainStandardService">
						<span class="glyphicon glyphicon-triangle-right"></span> 
						Maintain Standard Service Item - จัดการข้อมูลของรายการบริการมาตรฐาน
					</a>
				</td>
			</tr>
			<tr>
				<td>
					<a href="/admin/maintainOptionalService">
						<span class="glyphicon glyphicon-triangle-right"></span> 
						Maintain Optional Service - จัดการข้อมูลของรายการบริการเสริม (Option)
					</a>
				</td>
			</tr>
			<tr>
				<td>
					<a href="/admin/maintainPromotion">
						<span class="glyphicon glyphicon-triangle-right"></span> 
						Maintain Promotion Item - จัดการข้อมูลรายการส่งเสริมการขาย (Promotion)
					</a>
				</td>
			</tr>
			<tr>
				<td>
					<a href="/admin/maintainPricing">
						<span class="glyphicon glyphicon-triangle-right"></span> 
						Maintain Pricing - จัดการข้อมูลราคาและรายการการชำระ
					</a>
				</td>
			</tr>
			<tr>
				<td>
					<a href="/admin/maintainGeneralService">
						<span class="glyphicon glyphicon-triangle-right"></span> 
						Maintain General Service Item - จัดการข้อมูลรายการทั่วไป
					</a>
				</td>
			</tr>
			<tr>
				<td>
					<!-----------------  IN ORIGINAL WEB IT HAS NOTHING ---------------->
					<a href="#">
					<span class="glyphicon glyphicon-triangle-right"></span> 
						Generate Product Menu - การสร้างรายการสินค้าเพื่อลูกค้า (Product Menu)
					</a>
				</td>
			</tr>
			<tr class="active boldIndexMenu">
				<td> 
					Manage Package
				</td>
			</tr>
			<tr>
				<td>
					<a href="/admin/servicePackageList">
						<span class="glyphicon glyphicon-triangle-right"></span>
						Service Package List - รายการข้อมูลสินค้าจากรายการการบริการ
					</a>
				</td>
			</tr>
			<tr>
				<td>
					<a href="/admin/servicePackageForm">
						<span class="glyphicon glyphicon-triangle-right"></span>
						Add New Service Package - เพิ่มข้อมูลสินค้ารายการบริการ</a>
				</td>
			</tr>
			<tr class="active boldIndexMenu">
				<td> 
					Manage Service Contract
				</td>
			</tr>
			<tr>
				<td>
					<a href="/admin/serviceContractList">
						<span class="glyphicon glyphicon-triangle-right"></span>
						Service Contract List - ข้อมูลของการให้บริการและสัญญาการให้บริการ
					</a>	
				</td>
			</tr>
			<tr>
				<td>
					<a href="/admin/serviceContractForm">
						<span class="glyphicon glyphicon-triangle-right"></span>
						Add New Service Contract - เพิ่มข้อมูลของการให้บริการและสัญญาการให้บริการ
					</a>	
				</td>
			</tr>
			<tr class="active boldIndexMenu">
				<td>
					<a href="/admin" class="btn btn-danger btn-block">Log Off - ออกจากระบบ
					</a>
				</td>
			</tr>
		</tbody>
	</table>






@stop
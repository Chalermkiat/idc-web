@extends('header')

	@section('content')
		<h1>Form</h1>

		{!! Form::open(['url'=>'test']) !!}

			<div class="form-group">
				{!! Form::label('title','Title:')!!}
				{!! Form::text('title',null,['class'=>'form-control'])!!}
			</div>

			<div class="form-group">
				{!! Form::label('body','Body:')!!}
				{!! Form::textarea('body',null,['class'=>'form-control'])!!}
			</div>

			<div class="form-group">
				{!! Form::submit('Add Article',['class'=>'form-control btn btn-primary'])!!}
				
			</div>

		{!! Form::close() !!}

	@stop
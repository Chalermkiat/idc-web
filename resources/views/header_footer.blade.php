<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title> @yield('title') </title>
	
</head>
<body>
	<!-- Header -->
	<div class="header">			
		<a href="/"><img src="../images/pr_top2.jpg" class="img-fluid" alt="Responsive image" ></a>
	</div>


	
	
	<!-- yield Content -->
	<div class="container"> 
		
		<div class="row">
			<!-- BEGIN left Navigation -->
			<div class="col-sm-3 left-navi">
				<span >
					<h4>Products</h4>
				</span>
				<ul>

					<li><a  data-toggle="collapse" href="#collapseConfiguration" aria-expanded="false" aria-controls="collapseConfiguration">
						Configuration <span class="glyphicon glyphicon-triangle-bottom" aria-hidden="true"></span>
					</a></li>

					<div class="collapse" id="collapseConfiguration">

						<ul id="left-navi-list">
							<li><a href="/admin/hostingType">Maintain Hosting Type</a></li>
							<li><a href="/admin/hostingServerGroup">Maintain Server Group</a></li>
							<li><a href="/admin/hostingServer">Maintain Server</a></li>
							<li><a href="/admin/hostingServiceGroup">Maintain Service Group</a></li>
							<li><a href="/admin/maintainStandardService">Maintain Standard Service Item</a></li>
							<li><a href="/admin/maintainOptionalService">Maintain Optional Service</a></li>
							<li><a href="/admin/maintainPromotion">Maintain Promotion Item</a></li>
							<li><a href="/admin/maintainPricing">Maintain Pricing</a></li>
							<li><a href="/admin/maintainGeneralService">Maintain General Service Item</a></li>
							<li><a href="#">Generate Product Menu</a></li>
						</ul>

					</div>

					<li><a  data-toggle="collapse" href="#collapsePackage" aria-expanded="false" aria-controls="collapsePackage">
						Package <span class="glyphicon glyphicon-triangle-bottom" aria-hidden="true"></span>
					</a></li>

					<div class="collapse" id="collapsePackage">

						<ul id="left-navi-list">
							<li><a href="/admin/servicePackageList">Service Package List</a></li>
							<li><a href="/admin/servicePackageForm">Add New Service Package</a></li>		
						</ul>

					</div>

					<li><a data-toggle="collapse" href="#collapseService" aria-expanded="false" aria-controls="collapseService">
						Service Contract <span class="glyphicon glyphicon-triangle-bottom" aria-hidden="true"></span></a></li>

						<div class="collapse" id="collapseService">

							<ul id="left-navi-list">
								<li><a href="/admin/serviceContractList">Service Contract List</a></li>
								<li><a href="/admin/serviceContractForm">Add New Service Contract</a></li>		
							</ul>

						</div>

						<li><a href="/admin" id="logOff">Log Off</a></li>
					</ul>

				</div>

				<!-- BEGIN Content -->
				<div class="col-sm-9">
					<h2> ADMINISTRATION </h2>

					@yield('content') 
				</div>






			</div>
		</div>

		<!-- Footer -->
		<footer class="footer">
			<div class="container">

			</div>
		</footer>

		<!-- ==================Bootstrap core JavaScript =========================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<link rel="stylesheet" type="text/css" href={{ asset('/css/bootstrap.min.css') }}>
		<link rel="stylesheet" type="text/css" href={{ asset('/css/bootstrap-theme.min.css') }}>
		<link rel="stylesheet" type="text/css" href={{ asset('/css/idc-bob.css') }}>

		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>

		<!-- Latest compiled and minified JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
	</body>


	</html>